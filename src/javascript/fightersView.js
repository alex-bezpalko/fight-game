import View from './view';
import FighterView from './fighterView';
import { fighterService } from './services/fightersService';
import Fighter from './fighter';
import vsImg from '../../resources/vs.png';
import fightImg from '../../resources/fight.png';
import winnerImg from '../../resources/winner.jpeg';
import loserImg from '../../resources/loser.jpeg';

class FightersView extends View {
  constructor(fighters) {
    super();

    this.createFighters(fighters);

    this.vsImage = this.createVsImage(vsImg);
    this.fightImage = this.createFightImage(fightImg);
    this.winnerImage = this.createWinnerImage(winnerImg);
    this.loserImage = this.createLoserImage(loserImg);

    this.chosenFightersBlock = this.createChosenFightersBlock();

    this.fightParticipantsMap = new Map();
    this.fightersDetailsMap = new Map();
  }

  createFighters = fighters => {
    const fighterElements = fighters.map(fighter => {
      const fighterView = new FighterView(fighter, this.handleClick);
      return fighterView.element;
    });

    this.element = this.createElement({ tagName: 'div', className: 'fighters' });
    this.element.append(...fighterElements);
  }

  handleClick = async (event, fighter) => {
    let fighterDetails = null;
    if (this.fightersDetailsMap.has(fighter._id)){
      fighterDetails = this.fightersDetailsMap.get(fighter._id);      
    } else {
      fighterDetails = await fighterService.getFighterDetails(fighter._id);
      this.addFightPrepareSource(fighterDetails);
      this.fightersDetailsMap.set(fighter._id, fighterDetails);
    }
    if (fighterDetails) {
      this.openFighterCard(fighterDetails);
    }
  }

  addFightPrepareSource = fighterDetails => {
    const path = '../../resources/fightPrepareImages/';
    const fightPath = '../../resources/fightImages/';
    switch (fighterDetails.name) {
      case 'Ryu':
        fighterDetails.fightPrepareSource = `${path}Ryu.gif`;
        fighterDetails.fightLeftSideSource = `${fightPath}RyuLeftSide.gif`;
        fighterDetails.fightRightSideSource = `${fightPath}RyuRightSide.gif`;
        break;
      case 'Dhalsim':
        fighterDetails.fightPrepareSource = `${path}Dhalsim.gif`;
        fighterDetails.fightLeftSideSource = `${fightPath}DhalsimLeftSide.gif`;
        fighterDetails.fightRightSideSource = `${fightPath}DhalsimRightSide.gif`;
        break;
      case 'Guile':
        fighterDetails.fightPrepareSource = `${path}Guile.gif`;
        fighterDetails.fightLeftSideSource = `${fightPath}GuileLeftSide.gif`;
        fighterDetails.fightRightSideSource = `${fightPath}GuileRightSide.gif`;
        break;
      case 'Zangief':
        fighterDetails.fightPrepareSource = `${path}Zangief.gif`;
        fighterDetails.fightLeftSideSource = `${fightPath}ZangiefLeftSide.gif`;
        fighterDetails.fightRightSideSource = `${fightPath}ZangiefRightSide.gif`;
        break;
      case 'Ken':
        fighterDetails.fightPrepareSource = `${path}Ken.gif`;
        fighterDetails.fightLeftSideSource = `${fightPath}KenLeftSide.gif`;
        fighterDetails.fightRightSideSource = `${fightPath}KenRightSide.gif`;
        break;
      case 'Bison':
        fighterDetails.fightPrepareSource = `${path}Bison.gif`;
        fighterDetails.fightLeftSideSource = `${fightPath}BisonLeftSide.gif`;
        fighterDetails.fightRightSideSource = `${fightPath}BisonRightSide.gif`;
        break;
      default:
        fighterDetails.fightPrepareSource = fighterDetails.source;
        fighterDetails.fightLeftSideSource = fighterDetails.source;
        fighterDetails.fightRightSideSource = fighterDetails.source;
        break;
    }
  }

  openFighterCard = fighterDetails => {
    this.fillFighterDetails(fighterDetails);
    if (this.fightParticipantsMap.size > 1) {
      $('#chooseFighter').attr('disabled', true);
    }
    $('#chooseFighter').html('Choose the fighter');
    $('#chooseFighter').addClass('btn-success');
    $('#chooseFighter').removeClass('btn-danger');
    $('#chooseFighter').off('click');
    $('#chooseFighter').on('click', () => {
      if (this.fightParticipantsMap.size < 2) {
        const fighterView = new FighterView(fighterDetails, this.handleClickByChosen, true);
        const fighter = new Fighter(fighterDetails);
        const needImg = this.fightParticipantsMap.size === 0;
        this.fightParticipantsMap.set(fighter._id, fighter);
        if (needImg) {
          $('.fight').append(this.fightImage);
        }
        $('.fight').append(this.chosenFightersBlock);
        $('.fight-middle').append(fighterView.element);
        if (needImg) {
          $('.fight-middle').append(this.vsImage);
        }
        if (this.fightParticipantsMap.size === 2) {
          $('.fight').append(this.createStartButton());
        }
        $(`.fighters > #fighter-${fighterDetails._id}`).remove();
        $('#chooseModal').modal('hide');

      } else {
        $('.alert-danger').css('display', 'block');
        setTimeout(() => {
          $('.alert-danger').css('display', 'none');
        }, 500);
      }
    });
    this.saveFighterOnClick(fighterDetails);
    $('#chooseModal').modal('show');
  }

  handleClickByChosen = async (event, fighter) => {
    this.openChosenFighterCard(fighter);
  }

  openChosenFighterCard = fighterDetails => {
    this.fillFighterDetails(fighterDetails);
    $('#chooseFighter').attr('disabled', false);
    $('#chooseFighter').html('Remove from fight');
    $('#chooseFighter').addClass('btn-danger');
    $('#chooseFighter').removeClass('btn-success');
    $('#chooseFighter').off('click');
    $('#chooseFighter').on('click', () => {
      this.fightParticipantsMap.delete(fighterDetails._id);
      const fighterView = new FighterView(fighterDetails, this.handleClick);
      const needDeleteImg = this.fightParticipantsMap.size === 0;
      $('.fighters').append(fighterView.element);
      $(`.fight-middle > #fighter-${fighterDetails._id}`).remove();
      $('.fight-middle > .vs-image').remove();
      $('.fight > .btn-start').remove();
      if (needDeleteImg) {
        $('.fight > .fight-top').remove();
        $('.fight > .fight-middle').remove();
      } else {
        $('.fight-middle').append(this.vsImage);
      }
      $('#chooseModal').modal('hide');
    });
    this.saveFighterOnClick(fighterDetails);
    $('#chooseModal').modal('show');
  }

  fillFighterDetails = fighterDetails => {
    $('#fighterName').html(fighterDetails.name);
    $('input[name="health"]').val(fighterDetails.health);
    $('input[name="attack"]').val(fighterDetails.attack);
    $('input[name="defense"]').val(fighterDetails.defense);
  }

  saveFighterOnClick = fighterDetails => {
    $('#saveFighter').off('click');
    $('#saveFighter').on('click', () => {
      if (this.saveFighter(fighterDetails)){
        $('.alert-success').css('display', 'block');
        setTimeout(() => {
          $('.alert-success').css('display', 'none');
          $('#chooseModal').modal('hide');
        }, 500);
      } else {
        $('.alert-danger').css('display', 'block');
        setTimeout(() => {
          $('.alert-danger').css('display', 'none');
        }, 500);
      }
    });
  }

  saveFighter = fighterDetails => {
    const health = $('input[name="health"]').val();
    const attack = $('input[name="attack"]').val();
    const defense = $('input[name="defense"]').val();
    if (health < 1 || attack < 1 || defense < 1) {
      return false;
    } else {
      fighterDetails.health = health;
      fighterDetails.attack = attack;
      fighterDetails.defense = defense;
      const fighter = this.fightParticipantsMap.get(fighterDetails._id);
      if (fighter) {
        fighter.health = health;
        fighter.attack = attack;
        fighter.defense = defense;
      }
    }
    return true;
  }

  createVsImage = source => {
    const attributes = { src: source };
    const imgElement = this.createElement({
      tagName: 'img',
      className: 'vs-image',
      attributes
    });
    return imgElement;
  }

  createFightImage = source => {
    const divElement = this.createElement({
      tagName: 'div',
      className: 'fight-top',
    });
    const attributes = { src: source };
    const imgElement = this.createElement({
      tagName: 'img',
      className: 'fight-image',
      attributes
    });
    divElement.append(imgElement);
    return divElement;
  }

  createWinnerImage = source => {
    const divElement = this.createElement({
      tagName: 'div',
      className: 'fight-winner',
    });
    const attributes = { src: source };
    const imgElement = this.createElement({
      tagName: 'img',
      className: 'winner-image',
      attributes
    });
    divElement.append(imgElement);
    return divElement;
  }

  createLoserImage = source => {
    const divElement = this.createElement({
      tagName: 'div',
      className: 'fight-loser',
    });
    const attributes = { src: source };
    const imgElement = this.createElement({
      tagName: 'img',
      className: 'loser-image',
      attributes
    });
    divElement.append(imgElement);
    return divElement;
  }

  createChosenFightersBlock = () => {
    const element = this.createElement({
      tagName: 'div',
      className: 'fight-middle',
    });
    return element;
  }

  createStartButton = () => {
    const button = $('<button></button>').text('Start').addClass('btn btn-success btn-start');
    button.on('click', () => {
      if (this.fightParticipantsMap.size !== 2) {
        return;
      };
      const fighters = this.fightParticipantsMap.values();
      this.fight(fighters.next().value, fighters.next().value);
    });
    return button;
  }

  fight = (leftFighter, rightFighter) => {
    this.createFightField(leftFighter, rightFighter);

    let { health: leftFighterHealth } = leftFighter;
    let { health: rightFighterHealth } = rightFighter;
    
    const firstInterval = setInterval(() => {
      if (leftFighterHealth <= 0 || rightFighterHealth <= 0) {
        clearInterval(firstInterval);
        this.finishFight(leftFighterHealth, rightFighterHealth);
        return;
      }
      this.changeFightImage('left-side', leftFighter.fightLeftSideSource);
      setTimeout(() => {
        this.changeFightImage('left-side', leftFighter.fightPrepareSource);
      }, 750);
      const evasion = Math.floor(Math.random() * 101);
      // evasion near 20%
      if (evasion >= 0 && evasion <= 20) {
        return;
      }
      const attack = leftFighter.getHitPower();
      const defense = rightFighter.getBlockPower();
      const damage = (attack - defense) < 0 ? 0 : (attack - defense).toFixed(2);
      rightFighterHealth -= damage;
      this.changeHealthLine(rightFighter, rightFighterHealth < 0 ? 0 : rightFighterHealth.toFixed(2), 'right-fighter');
    }, 1500);
    setTimeout(() => {
      const secondInterval = setInterval(() => {
        if (leftFighterHealth <= 0 || rightFighterHealth <= 0) {
          clearInterval(secondInterval);
          return;
        }
        this.changeFightImage('right-side', rightFighter.fightRightSideSource);
        setTimeout(() => {
          this.changeFightImage('right-side', rightFighter.fightPrepareSource);
        }, 750);
        const evasion = Math.floor(Math.random() * 101);
        // evasion near 20%
        if (evasion >= 0 && evasion <= 20) {
          return;
        }
        const attack = rightFighter.getHitPower();
        const defense = leftFighter.getBlockPower();
        const damage = (attack - defense) < 0 ? 0 : (attack - defense).toFixed(2);
        leftFighterHealth -= damage;
        this.changeHealthLine(leftFighter, leftFighterHealth < 0 ? 0 : leftFighterHealth.toFixed(2), 'left-fighter');
      }, 1500);
    }, 750);
  }

  createFightField = (leftFighter, rightFighter) => {
    $('#root > .fight').remove();
    $('#root > .fighters').remove();
    $('.fight-field').css('display', 'flex').prepend(this.fightImage);

    this.makeHealthLine(leftFighter, 'bg-success left-fighter', 'left-side');
    this.makeHealthLine(rightFighter, 'bg-warning right-fighter', 'right-side');

    const leftFighterView = new FighterView(leftFighter, () => {}, true);
    const rightFighterView = new FighterView(rightFighter, () => {}, true);

    $('.participants > .left-side').append(leftFighterView.element);
    $('.participants > .right-side').append(rightFighterView.element);
    
  }

  makeHealthLine = (fighter, barClass, side) => {
    const { health } = fighter;
    const progress = $('<div></div>').addClass('progress').attr('style', 'width: 100%');
    const progressBar = $('<div></div>')
      .addClass(`progress-bar progress-bar-striped ${barClass}`)
      .attr({
        role: 'progressbar',
        style: `width: 100%`,
        'aria-valuenow': health,
        'aria-valuemin': "0",
        'aria-valuemax': health,
      })
      .text(`${health} / ${health}`);
    progress.append(progressBar);
    $(`.participants > .${side}`).append(progress);
  }

  changeHealthLine = (fighter, health, fighterClass) => {
    const { health: fullHealth } = fighter;
    $(`.participants .${fighterClass}`)
      .attr({
        role: 'progressbar',
        style: `width: ${health/fullHealth*100}%`,
        'aria-valuenow': health,
        'aria-valuemin': "0",
        'aria-valuemax': fullHealth,
      })
      .text(`${health} / ${fullHealth}`);
  }

  changeFightImage = (side, sourse) => {
    $(`.participants > .${side} .fighter-image`).attr('src', sourse);
  }

  finishFight = (leftFighterHealth, rightFighterHealth) => {
    if (leftFighterHealth <= 0) {
      $('.participants > .left-side').prepend(this.loserImage);
      $('.participants > .right-side').prepend(this.winnerImage);
    }
    if (rightFighterHealth <= 0) {
      $('.participants > .left-side').prepend(this.winnerImage);
      $('.participants > .right-side').prepend(this.loserImage);
    }
    const button = $('<button></button>').text('Finish').addClass('btn btn-dark btn-finish');
    button.on('click', () => {
      document.location.reload(true);
    });
    $('.fight-field').append(button);
  }
}

export default FightersView;
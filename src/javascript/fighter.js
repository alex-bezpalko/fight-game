export default class Fighter {
  constructor(fighterDetails) {
    this._id = fighterDetails._id;
    this.name = fighterDetails.name;
    this.health = fighterDetails.health;
    this.attack = fighterDetails.attack;
    this.defense = fighterDetails.defense;
    this.source = fighterDetails.source;
    this.fightPrepareSource = fighterDetails.fightPrepareSource;
    this.fightLeftSideSource = fighterDetails.fightLeftSideSource;
    this.fightRightSideSource = fighterDetails.fightRightSideSource;
  }

  getHitPower = () => {
    return this.attack * (Math.random() + 1);
  }

  getBlockPower = () => {
    return this.defense * (Math.random() + 1);
  }
}
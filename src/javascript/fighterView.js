import View from './view';

class FighterView extends View {
  constructor(fighter, handleClick, fightPrepare = false) {
    super();

    this.createFighter(fighter, handleClick, fightPrepare);
  }

  createFighter(fighter, handleClick, fightPrepare) {
    const { name, source, fightPrepareSource } = fighter;
    const nameElement = this.createName(name);
    let imageElement;
    if (fightPrepare) {
      imageElement = this.createImage(fightPrepareSource);
    } else {
      imageElement = this.createImage(source);
    }

    this.element = this.createElement({ tagName: 'div', className: 'fighter', attributes: {id: `fighter-${fighter._id}`} });
    this.element.append(imageElement, nameElement);
    this.element.addEventListener('click', event => handleClick(event, fighter), false);
  }

  createName(name) {
    const nameElement = this.createElement({ tagName: 'span', className: 'name' });
    nameElement.innerText = name;

    return nameElement;
  }

  createImage(source) {
    const attributes = { src: source };
    const imgElement = this.createElement({
      tagName: 'img',
      className: 'fighter-image',
      attributes
    });

    return imgElement;
  }
}

export default FighterView;